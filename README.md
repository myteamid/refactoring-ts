1. Install the dependencies with this command: `npm install`
2. Run the game with this command: `npm run start`
3. Run the tests with this command: `npm test`

Any test source matching the pattern `*.test.ts` below `tests/` will be executed.

Task:
1) Refactor according to received knowledges;
2) Write tests.

Deadline: 16.01.2019 23:59
Name the folder with your h/t as "Name_Surname-Clean-Code-Task" and put it at the same level with current folder
