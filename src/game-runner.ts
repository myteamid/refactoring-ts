import {Game} from './game';
import {people} from "./people";

export class GameRunner {
    public static main(): void {
        const game = new Game();

        people.map(item => game.add(item));

        let notAWinner;
        do {

            game.roll(Math.floor(Math.random() * 6) + 1);

            if (Math.floor(Math.random() * 10) == 7) {
                notAWinner = game.wrongAnswer();
            } else {
                notAWinner = game.wasCorrectlyAnswered();
            }

        } while (notAWinner);

        console.log('-----Game is finished!-----');

    }
}

GameRunner.main();

