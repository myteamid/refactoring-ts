export class Game {

    private players: Array<string> = [];
    private places: Array<number> = [];
    private purses: Array<number> = [];
    private inPenaltyBox: Array<boolean> = [];
    private currentPlayer: number = 0;
    private isGettingOutOfPenaltyBox: boolean = false;
    private questionsLength: number = 50;
    private maxLengthPlayers: number = 11;
    private questionsType: Array<string> = ["Pop", "Science", "Sports", "Rock"];
    private popQuestions: Array<string> = [];
    private scienceQuestions: Array<string> = [];
    private sportsQuestions: Array<string> = [];
    private rockQuestions: Array<string> = [];

    constructor() {
        this.addArrayQuestions(this.questionsLength)
    }

    public add(name: string): boolean {
        this.players.push(name);
        this.places[this.howManyPlayers() - 1] = 0;
        this.purses[this.howManyPlayers() - 1] = 0;
        this.inPenaltyBox[this.howManyPlayers() - 1] = false;

        console.log(`${name} was added`);
        console.log(`They are player number ${this.howManyPlayers()}`);

        return true;
    }

    public roll(roll: number) {
        console.log(`${this.players[this.currentPlayer]} is the current player`);
        console.log(`They have rolled a ${roll}`);

        if (this.inPenaltyBox[this.currentPlayer] && roll % 2 != 0) {

            this.isGettingOutOfPenaltyBox = true;
            console.log(`${this.players[this.currentPlayer]} is getting out of the penalty box`);

        } else {

            console.log(`${this.players[this.currentPlayer]} is not getting out of the penalty box`);
            this.isGettingOutOfPenaltyBox = false;
        }

        this.places[this.currentPlayer] += roll;
        if (this.places[this.currentPlayer] > this.maxLengthPlayers) {
            this.places[this.currentPlayer] -= this.maxLengthPlayers + 1;
        }
        console.log(`${this.players[this.currentPlayer]} 's new location is ${this.places[this.currentPlayer]}`);
        console.log(`The category is ${this.currentCategory()}`);
        this.askQuestion();
    }

    public wrongAnswer(): boolean {
        console.log('Question was incorrectly answered');
        console.log(`${this.players[this.currentPlayer]} was sent to the penalty box`);

        this.inPenaltyBox[this.currentPlayer] = true;
        this.incrementPlayer();
        this.resetCurrentPlayer();

        return true;
    }

    public wasCorrectlyAnswered(): boolean {
        if (this.inPenaltyBox[this.currentPlayer] && !this.isGettingOutOfPenaltyBox) {
            this.incrementPlayer();
            this.resetCurrentPlayer();

            return true;
        }

        console.log("Answer was correct!!!!");
        this.purses[this.currentPlayer] += 1;
        console.log(`${this.players[this.currentPlayer]} now has ${this.purses[this.currentPlayer]} + Gold Coins.`);
        this.incrementPlayer();
        this.resetCurrentPlayer();

        return this.didPlayerWin();
    }

    private addArrayQuestions(length: number) {
        for (let i = 0; i < length; i++) {
            this.popQuestions.push(`Pop Question ${i}`);
            this.scienceQuestions.push(`Science Question ${i}`);
            this.sportsQuestions.push(`Sports Question ${i}`);
            this.rockQuestions.push(`Rock Question ${i}`);
        }
    }

    private howManyPlayers(): number {
        return this.players.length;
    }

    private askQuestion(): void {
        if (this.currentCategory() == 'Pop')
            console.log(this.popQuestions.shift());

        if (this.currentCategory() == 'Science')
            console.log(this.scienceQuestions.shift());

        if (this.currentCategory() == 'Sports')
            console.log(this.sportsQuestions.shift());

        if (this.currentCategory() == 'Rock')
            console.log(this.rockQuestions.shift());
    }

    private currentCategory(): string {
        const currentPlace = this.places[this.currentPlayer];
        const type = {
            [this.questionsType[0]]: [0, 4, 8],
            [this.questionsType[1]]: [1, 5, 9],
            [this.questionsType[2]]: [2, 6, 10],
            [this.questionsType[3]]: [3, 7, 11]
        };

        const type2 = {
            [this.questionsType[0]]: {
                arr: this.popQuestions,
                id: [0, 4, 8]
            },
            [this.questionsType[1]]: [1, 5, 9],
            [this.questionsType[2]]: [2, 6, 10],
            [this.questionsType[3]]: [3, 7, 11]
        };

        const findType = (num: number): string => {
            if (num > this.maxLengthPlayers) return this.questionsType[3];

            for (let elem in type) {
                const currentArr = type[elem].filter(item => num === item);
                if (currentArr.length) return elem;
            }
        };

        return findType(currentPlace);

    }

    private didPlayerWin(): boolean {
        return !(this.purses[this.currentPlayer] == 6)
    }

    private resetCurrentPlayer(): void {
        if (this.currentPlayer == this.howManyPlayers())
            this.currentPlayer = 0;
    }

    private incrementPlayer(): void {
        this.currentPlayer += 1;
    }

}
